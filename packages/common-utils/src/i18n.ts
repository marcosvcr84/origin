import Vue from 'vue';
import dayjs from 'dayjs';
import VueI18n, { LocaleMessages } from 'vue-i18n';
import { localize } from 'vee-validate';

Vue.use(VueI18n);

function getBrowserLanguage(): string {
  return window.navigator.language.split('-')[0].toLowerCase();
}

const currentUserLanguage = getBrowserLanguage();

function loadLocaleMessages(): LocaleMessages {
  const locales = require.context('./locales', false, /[A-Za-z0-9-_,\s]+\.json$/i);
  const messages: LocaleMessages = {};
  locales.keys().forEach((key: string) => {
    const matched = key.match(/([A-Za-z0-9-_]+)\./i);
    if (matched && matched.length > 1) {
      const locale = matched[1];
      messages[locale] = locales(key);
    }
  });
  return messages;
}

const i18nInstance = new VueI18n({
  locale: currentUserLanguage || process.env.VUE_APP_I18N_LOCALE || 'en',
  fallbackLocale: process.env.VUE_APP_I18N_FALLBACK_LOCALE || 'en',
  messages: loadLocaleMessages()
});

function setI18nLanguage(lang: string): void {
  i18nInstance.locale = lang;
  localize(lang);
  dayjs.locale(lang);
  document.querySelector('html')!.setAttribute('lang', lang);
}

export default i18nInstance;

export { setI18nLanguage };
