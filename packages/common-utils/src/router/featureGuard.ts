import { NavigationGuardNext, Route } from 'vue-router';
import { Store } from 'vuex';

const featureGuard = async (
  to: Route,
  from: Route,
  next: NavigationGuardNext,
  store: Store<any> // Vuex.Store
): Promise<void> => {
  if (to.meta?.publicAccess) {
    next();
  }
};

export default featureGuard;
