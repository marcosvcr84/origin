import { NavigationGuardNext, Route, RouteRecord } from 'vue-router';
import { Store } from 'vuex';

export default async function processRoute(
  to: Route,
  from: Route,
  next: NavigationGuardNext,
  store: Store<any>, // Vuex.Store
): Promise<void> {

  // If the route is public, let it pass
  if (to.matched.some((record: RouteRecord) => record.meta.publicAccess)) {
    return next();
  }

  return next();
}
