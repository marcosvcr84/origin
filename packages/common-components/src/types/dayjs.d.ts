import { Dayjs } from 'dayjs';

declare module 'vue/types/vue' {
  interface Vue {
    $date: Dayjs;
  }
}
