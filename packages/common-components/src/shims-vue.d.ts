declare module '*.vue' {
  import Vue from 'vue';

  export default Vue;
}

declare module 'vee-validate';
declare module 'vee-validate/dist/locale/en.json';
declare module 'vee-validate/dist/rules';
declare module 'vue-inline-svg';
declare module 'vue-property-decorator';
declare module 'vue-advanced-cropper';
declare module 'vue2-editor';
declare module 'lodash';

declare module '*.json';
declare module '*.scss';

declare module '*.svg' {
  const content: any;
  export default content;
}
