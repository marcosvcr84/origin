interface BaseFormControlInt {
  validate(): boolean;
}
export default BaseFormControlInt;
