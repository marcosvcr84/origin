declare module '*.vue' {
  import Vue from 'vue';

  export default typeof Vue;
}

declare module 'vue-property-decorator';
declare module 'vue-inline-svg';
declare module 'vue-advanced-cropper';
declare module 'debounce';
declare module 'lodash';

declare module '*.json';
declare module '*.scss';

declare module '*.svg' {
  const content: any;
  export default content;
}
