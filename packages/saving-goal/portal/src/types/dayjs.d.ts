import { ConfigType, Dayjs, OptionType } from 'dayjs';

declare module 'vue/types/vue' {
  interface Vue {
    $date(date?: ConfigType, option?: OptionType, locale?: string): Dayjs;
  }
}
