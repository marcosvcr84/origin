import { NavigationGuardNext, Route, RouteConfig } from 'vue-router';
import VueRouter from '@improve/common-utils/src/router/index';
import store from '../store';

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'Dashboard',
    meta: {
      publicAccess: true
    }
  },
  {
    path: '*',
    redirect: '/',
    meta: {
      publicAccess: true
    }
  }
]
  .map((route: RouteConfig) => ({
    component: () => import(/* webpackChunkName: "[request]" */ `../views/${route.name}.vue`),
    ...route
  }));

const router = VueRouter(routes, store);
router.beforeEach(async (
  to: Route,
  from: Route,
  next: NavigationGuardNext
) => next());

export default router;
