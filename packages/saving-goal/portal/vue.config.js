/* eslint-disable @typescript-eslint/no-var-requires */
process.env.VUE_APP_VERSION = require('./package.json').version;
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');

module.exports = {
  devServer: {
    port: 5000,
    compress: true,
    headers: { 'Access-Control-Allow-Origin': '*' }
  },
  pluginOptions: {
    i18n: {
      locale: 'en',
      fallbackLocale: 'en',
      localeDir: 'locales',
      enableInSFC: true
    }
  },
  configureWebpack: {
    entry: './src/main.ts',
    resolve: {
      alias: {
        vue$: 'vue/dist/vue.esm.js'
      },
      extensions: ['.ts', '.vue', '.js']
    },
    plugins: [
      new BundleAnalyzerPlugin({
        openAnalyzer: false,
        analyzerMode: 'static'
      })
    ]
  },
  css: {
    loaderOptions: {
      scss: {
        additionalData: '@import "~@/assets/theme/origin-portal.scss";'
      }
    }
  },
  transpileDependencies: [
    'vuetify'
  ]
};
