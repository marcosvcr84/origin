module.exports = {
  trailingComma: 'none',
  useTabs: false,
  tabWidth: 2,
  semi: true,
  singleQuote: true,
  quoteProps: 'as-needed',
  bracketSpacing: true,
  vueIndentScriptAndStyle: false,
  htmlWhitespaceSensitivity: 'strict',
  arrowParens: 'always',
  endOfLine: 'lf',
  printWidth: 80
};
