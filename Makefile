copy-fonts:
	for app in "improve/portal" "improve/admin"; do \
		mkdir -p "packages/$${app}/public/fonts" ; \
		cp -R "packages/common-components/src/assets/fonts/." "packages/$${app}/public/fonts" ; \
	done

improve-portal-container:
	docker-compose -f docker-compose.yml build improve-portal

improve-admin-container:
	docker-compose -f docker-compose.yml build improve-admin

all-containers: improve-admin-container improve-portal-container
	echo "Build all containers"

run-improve-portal: improve-portal-container
	docker-compose -f docker-compose.yml up -d improve-portal

run-improve-admin: improve-admin-container
	docker-compose -f docker-compose.yml up -d improve-admin

run-all: run-improve-admin run-improve-portal
	echo "Run all containers"

stop-all:
	docker-compose -f docker-compose.yml stop
	docker-compose -f docker-compose.yml rm -fv

ps:
	docker-compose -f docker-compose.yml ps
